{
  inputs.nci.url = "github:yusdacra/nix-cargo-integration";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-21.11";

  outputs = inputs:
    inputs.nci.lib.makeOutputs {
      root = ./.;
      overrides = {
        shell = common: prev: {
          packages = [
            inputs.nixpkgs.legacyPackages.x86_64-linux.nix
          ];
        };
      };
    };
}
